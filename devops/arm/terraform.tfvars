vnet_rg = "prod-zah-network-rg"

vnet_name = "it-prod-ip-2"
address_space = "172.17.251.64/27"
dns_servers = ["172.17.253.7",
               "172.17.253.8"]

subnet_name = ["default"]
address_prefix = ["172.17.251.64/27"]

location = "eastus"


tags = {
  Department     = "IT Cloud Data"
  Environment    = "Sandbox"
  Tier           = "4"
  Owner          = "Mac Rice"
  Project        = "Cloud Data"
  Application    = "TEST Azure Foundation"
  DataProfile    = ""
  BackupSchedule = ""
  PatchSchedule  = ""
}
